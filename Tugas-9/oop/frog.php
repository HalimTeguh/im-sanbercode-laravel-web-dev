<?php 

class Frog extends Animal{

    public function __construct($name){
        parent::__construct($name);
    }
   
    public function jump():void {
        echo "Hop Hop";
    }

}