<?php

class Animal{
    private $name;
    private $legs;
    private $cold_blooded;

    public function __construct($name){
        $this->name = $name;
        $this->legs = 4;
        $this->cold_blooded = "no";
    }

    public function getName(): String{
        return $this->name;
    }

    public function setName($name): void{
        $this->name = $name;
    }

    public function getLegs(): int{
        return $this->legs;
    }

    public function setLegs($legs): void{
        $this->legs = $legs;
    }

    public function getCold_blooded(): String{
        return $this->cold_blooded;
    }

    public function setCold_blooded($status): void{
        $this->cold_blooded = $status;
    }

}