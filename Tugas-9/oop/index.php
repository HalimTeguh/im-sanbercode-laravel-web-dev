<?php

require('animal.php');
require('ape.php');
require('frog.php');


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP OOP</title>
</head>
<body>
    <h1>PHP OOP</h1>

    <h3>Release 0 - Membuat Class Animal</h3>

    <?php
        $sheep = new Animal("shaun");

        echo "Name : " . $sheep->getName() . "<br>";
        echo "Legs : " . $sheep->getLegs() . "<br>";
        echo "Cold Blooded : " . $sheep->getCold_blooded() . "<br>"; 
    ?>

    <h3>Release 1 - Membuat Class Animal</h3>
    
    <h4>Kodok</h4>
    <?php
        $kodok = new Frog("Buduk");
    
        echo "Name : " . $kodok->getName() . "<br>";
        echo "legs : " . $kodok->getLegs() . "<br>";
        echo "Cold Blooded : " . $kodok->getCold_blooded() . "<br>";
        echo "Jump : ";
        $kodok->jump();
    ?>
        
    <h4>Kera</h4>
    <?php
        $sungokong = new Ape("Kera Sakti");

        echo "Name : " . $sungokong->getName() . "<br>";
        echo "Legs : " . $sungokong->getLegs() . "<br>";
        echo "Cold Blooded : " . $sungokong->getCold_blooded() . "<br>";
        echo "Yell : ";
        $sungokong->yell();
    ?>

</body>
</html>