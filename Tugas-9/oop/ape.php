<?php 

class Ape extends Animal{
    public $legs = 2;

    public function __construct($name){
        parent::__construct($name);
        parent::setLegs(2);
    }
   
    public function yell():void {
        echo "Auoooo";
    }

}