<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function form(){
        return view('pages.form', ['title' => 'register']);
    }

    public function send(Request $request){
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $name = "$firstname $lastname";
        $gender = $request->input('gender');
        $nation = $request->input('nation');
        $language = $request->input('language');
        $bio = $request->input('bio');

        return view('pages.welcome', ['title' => 'dashboard', 'name' => $name, 'gender' => $gender, 'nation' => $nation, 'language' => $language, 'bio' => $bio]);

    }
}
