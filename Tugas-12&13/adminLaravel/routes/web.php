<?php

use App\Http\Controllers\authController;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [homeController::class, 'index']);

Route::get('/register', [authController::class, 'form']);

Route::post('/welcome', [authController::class, 'send']);

Route::get('/table', function (){
    return view('pages.tables', ['title' => 'tables']);
});

Route::get('/data-tables', function (){
    return view('pages.dataTables', ['title' => 'dataTables']);
});