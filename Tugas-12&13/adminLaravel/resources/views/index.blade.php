@extends('master')

@section('title')
SanberBook
@endsection


@section('content')

<div class="col-lg-6">

    <!-- Default Card Example -->
    <div class="card mb-4">
        <div class="card-header">
            Social Media Developer Santai Berkualitas
        </div>
        <div class="card-body">
            Belajar dan berbagi agar hidup ini semakin santai berkualitas
        </div>
    </div>

    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Benefit Join di SanberBook</h6>
        </div>
        <div class="card-body">
            <ul>
                <li>Mendapatkan Motivasi dari sesama developer</li>
                <li>Sharing knowledge dare para mastah Sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
        </div>
    </div>

</div>

<div class="col-lg-6">

    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Cara Bergabung ke SanberBook</h6>
        </div>
        <div class="card-body">
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
        </div>
    </div>

</div>
    
@endsection
