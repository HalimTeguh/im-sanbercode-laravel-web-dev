@extends('master')

@section('title')
Buat Account Baru!
@endsection

@section('content')
<div class="col-lg-12">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Sign Up Form</h6>
        </div>

        <div class="card-body">
            <form action="/welcome" method="post">
                @csrf

                <div class="row">

                    <div class="col-lg-6">
                        <label for="firstname" class="form-label">First name:</label>
                        <input class="form-control" type="text" name="firstname" id="firstname">
                    </div>
    
                    <div class="col-lg-6">
                        <label for="lastname" class="form-label">Last name:</label>
                        <input class="form-control" type="text" name="lastname" id="lastname">
                    </div>
    
                </div>

            <br>         

            <div class="row">

                <div class="col-lg-6">
                    <label class="form-label" for="gender">Gender:</label><br>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" id="gender" value="Male">
                        <label class="form-check-label" for="gender">
                            Male
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" id="gender" value="Female">
                        <label class="form-check-label" for="gender">
                            Female
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gender" id="gender" value="Other">
                        <label class="form-check-label" for="gender">
                            Other
                        </label>
                    </div>
                </div>

                <div class="col-lg-6">
                    <label class="form-label" for="language">Language Spoken:</label>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="language" id="language" value="Bahasa Indonesia">
                        <label class="form-check-label" for="gender">
                            Bahasa Indonesia
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="language" id="language" value="English">
                        <label class="form-check-label" for="gender">
                            English
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="language" id="language" value="Other">
                        <label class="form-check-label" for="gender">
                            Other
                        </label>
                    </div>
                </div>

            </div>

            <br>         
        
            <label class="form-label" for="nation">Nationally:</label>
            <select class="form-control" name="nation" id="nation">
                <option class="dropdown-item" value="#">Choose Your Nation</option>
                <option class="dropdown-item" value="Indonesia">Indonesia</option>
                <option class="dropdown-item" value="Malaysia">Malaysia</option>
                <option class="dropdown-item" value="Singapura">Singapura</option>
                <option class="dropdown-item" value="Jepang">Jepang</option>
            </select>

            <br>
            
            <label class="form-label" for="bio">Bio:</label>
            <textarea class="form-control" name="bio" id="bio" cols="20" rows="10"></textarea><br><br>

            <input class="btn btn-info text-white" type="submit" value="Submit">
            
            
            </form>
        </div>
    </div>

</div>
@endsection