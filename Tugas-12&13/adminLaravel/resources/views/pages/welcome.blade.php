@extends('master')

@section('title')
    Welcome
@endsection

@section('content')
<div class="card mb-4">
    <div class="card-header">
        Selamat Datang! {{ $name }}
    </div>
    <div class="card-body">
        <h2 class="mb-3">Terima kasih telah bergabung di sanberbook. Social Media kita bersama!</h2>
    
        <a href="/">Kembali ke halaman utama</a>
    </div>
</div>
@endsection
