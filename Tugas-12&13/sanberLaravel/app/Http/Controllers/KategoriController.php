<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kategori = Kategori::all();
        return view('pages.kategori', ['title' => 'kategori', 'kategori' => $kategori]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.formAddKategori', ['title' => 'kategori',]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($validation == true){
            $kategori = new Kategori;
            $kategori->name = $request->name;
            $kategori->description = $request->description;

            $kategori->save();

            return redirect('/kategori');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Kategori $kategori)
    {
        return view('pages.detailKategori', ['title' => 'kategori', 'kategori' => $kategori]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kategori $kategori)
    {
        return view('pages.formEditKategori', ['title' => 'kategori', 'kategori' => $kategori]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kategori $kategori)
    {
        $kategori->name = $request->name;
        $kategori->description = $request->description;

        $kategori->save();
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kategori $kategori)
    {
        $kategori->delete();

        return redirect('/kategori');

    }
}
