<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function index(){

        $game = DB::table('game')->get();

        return view('pages.game', ['game' => $game]);
    }
    
    public function create(){
        return view('pages.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        DB::table('game')->insert([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year'],
        ]);

        return redirect('/game');

    }

    public function show($id){
        $game = DB::table('game')->where('id', $id)->first();

        return view('pages.detail', ['game' => $game]);
    }

    public function edit($id){
        $game = DB::table('game')->where('id', $id)->first();

        return view('pages.formEditgame', ['game' => $game]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        DB::table('game')
            ->where('id', $id)
            ->update([
                'name' => $request->name,
                'gameplay' => $request->gameplay,
                'developer' => $request->developer,
                'year' > $request->year
            ]);

        return redirect('/game');
    }

    public function destroy($id){
        DB::table('game')->where('id', $id)->delete();

        return redirect('/game');
    }

}
