<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){

        $cast = DB::table('cast')->get();

        return view('pages.cast', ['title' => 'cast', 'cast' => $cast]);
    }

    public function create(){
        return view('pages.formAddCast',['title' => 'cast']);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'name' => $request['name'],
            'umur' => $request['age'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');

    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('pages.detail', ['title' => 'cast', 'cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('pages.formEditCast', ['title' => 'cast', 'cast' => $cast]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'name' => $request->name,
                'umur' => $request->age,
                'bio' => $request->bio
            ]);

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
