@extends('master')

@section('title')
    SELAMAT DATANG! {{ $name }}
@endsection

@section('page1')
    register / welcome
@endsection

@section('subtitle')
    Welcome
@endsection

@section('content')
    <h2 class="mb-3">Terima kasih telah bergabung di sanberbook. Social Media kita bersama!</h2>

    <a href="/">Kembali ke halaman utama</a>
@endsection
