@extends('master')

@section('title')
    Cast
@endsection

@section('subtitle')
    Form Add Cast    
@endsection

@section('page1')
    Cast / AddCast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
    </div>
    @error('name')
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="form-group">
        <label for="age">Age</label>
        <input type="number" class="form-control" id="age" name="age" placeholder="Enter Age">
    </div>
    @error('age')
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection