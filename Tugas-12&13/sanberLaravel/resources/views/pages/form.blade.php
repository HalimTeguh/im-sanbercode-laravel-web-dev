@extends('master')

@section('title')
Buat Account Baru
@endsection

@section('page1')
register
@endsection

@section('subtitle')    
Sign Up Form
@endsection

@section('content')
    
    <form action="/welcome" method="post">
        @csrf

        <label for="firstname" class="form-label">First name:</label>
        <input class="form-control" type="text" name="firstname" id="firstname">

        <br>

        <label for="lastname" class="form-label">Last name:</label>
        <input class="form-control" type="text" name="lastname" id="lastname">

        <br>

        <label class="form-label" for="gender">Gender:</label><br>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="gender" id="gender" value="Male">
            <label class="form-check-label" for="gender">
                Male
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="gender" id="gender" value="Female">
            <label class="form-check-label" for="gender">
                Female
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="gender" id="gender" value="Other">
            <label class="form-check-label" for="gender">
                Other
            </label>
        </div>

        <br><br>

        <label class="form-label" for="nation">Nationally:</label>
        <select class="form-control" name="nation" id="nation">
            <option value="#">Choose Your Nation</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
            <option value="Jepang">Jepang</option>
        </select>

        <br>

        <label class="form-label" for="language">Language Spoken:</label>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="language" id="language" value="Bahasa Indonesia">
            <label class="form-check-label" for="gender">
                Bahasa Indonesia
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="language" id="language" value="English">
            <label class="form-check-label" for="gender">
                English
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="language" id="language" value="Other">
            <label class="form-check-label" for="gender">
                Other
            </label>
        </div>

        <br><br>

        <label class="form-label" for="bio">Bio: </label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

        <input class="btn btn-info text-white" type="submit" value="Submit">


    </form>
@endsection
