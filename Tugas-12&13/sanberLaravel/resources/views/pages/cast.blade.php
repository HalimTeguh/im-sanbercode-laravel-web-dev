@extends('master')

@section('title')
    Cast    
@endsection

@section('subtitle')
    Table Cast
@endsection

@section('page1')
    Cast
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@push('scripts')
    <script src="{{ asset('/template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush

@section('content')
<a href="/cast/create" class="btn btn-primary mt-0 mb-3 ml-3">Add Cast</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>id</th>
      <th>Name</th>
      <th>Age</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $row)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $row->name }}</td>
            <td>{{ $row->umur }}</td>
            <td>
              <a href="/cast/{{ $row->id }}" class="btn btn-info m-1 w-100">Detail</a><br>
              <a href="/cast/{{ $row->id }}/edit" class="btn btn-warning m-1 w-100">Edit</a>
              <form action="/cast/{{ $row->id }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger m-1 w-100">Delete</button>
              </form>
            </td>
          </tr>
      @empty
          <p>No users</p>
      @endforelse
    </tbody>
    <tfoot>
      <tr>
        <th>id</th>
        <th>Name</th>
        <th>Age</th>
        <th>Action</th>
      </tr>
    </tfoot>
  </table>
@endsection