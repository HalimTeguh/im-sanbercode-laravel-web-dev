@extends('master')

@section('title')
    Cast
@endsection

@section('subtitle')
    Detail Cast 
@endsection

@section('page1')
    Cast / DetailCast
@endsection

@section('content')
<form action="/cast" method="get">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ $cast->name }}" readonly>
    </div>
    <div class="form-group">
        <label for="age">Age</label>
        <input type="number" class="form-control" id="age" name="age" value="{{ $cast->umur }}" readonly>
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10" readonly>{{ $cast->bio }}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Back</button>
  </form>
@endsection