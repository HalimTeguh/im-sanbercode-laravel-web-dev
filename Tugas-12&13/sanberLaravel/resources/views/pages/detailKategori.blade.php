@extends('master')

@section('title')
    Kategori
@endsection

@section('subtitle')
    Form Edit Kategori    
@endsection

@section('page1')
    Kategori / EditKategori
@endsection

@section('content')
<form action="/kategori" method="GET">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{ $kategori->name }}" readonly>
    </div>
    @error('name')
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" id="description" cols="30" rows="10" readonly>{{ $kategori->description }}</textarea>
    </div>
    @error('description')
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
    @enderror
    <button type="submit" class="btn btn-danger">Back</button>
  </form>
@endsection