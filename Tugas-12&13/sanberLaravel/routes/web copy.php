<?php

use App\Http\Controllers\authController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [homeController::class, 'index']);

Route::get('/register', [authController::class, 'form']);

Route::post('/welcome', [authController::class, 'send']);

Route::get('/table', function (){
    return view('pages.table', ['title' => 'table']);
});

Route::get('/data-tables', function (){
    return view('pages.data-tables' , ['title' => 'data-tables']);
});

//Cast CRUD
Route::get('/game', [GameController::class, 'index']);

Route::get('/game/create', [GameController::class, 'create']);
Route::post('/game', [GameController::class, 'store']);

Route::get('/game/{game_id}', [GameController::class, 'show']);

Route::get('/game/{game_id}/edit', [GameController::class, 'edit']);
Route::put('/game/{game_id}', [GameController::class, 'update']);

Route::delete('/game/{game_id}', [GameController::class, 'destroy']);